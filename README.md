# Greenhouse Stage Notities #
In dit repository ga ik de notities die ik maak opslaan om ze gemakkelijk te kunnen delen. 

# Wekelijkse Notities #

### Week 1 ###
Week 1 bestond voor mij voor het grootste deel uit kennismaking met mijn mede labs collega's. Na 2 dagen introductie begon labs "for real".

Na kennis gemaakte te hebben met de Deense studenten en een introductie presentatie begonnen we met het project. De rest van de eerste week hebben we elkaar beter leren kennen en hebben we de basisregels van het project gedefinieerd.

Ik ben erg blij met de manier waarop het project ingedeeld is. Het gebruik van Scrum en het vooraf opzetten van een groepscontract gaf ons een goed inzicht van onze eigen verwachtingen en die van de stakeholders.

### Week 2 ###
In week 2 zijn we begonnen met het brainstormen en vullen van onze backlog. Mijn focus hierin lag op het technische deel van NFT's en de onderliggende blockchain. Ik had zelf al wat kennis van de blockchains zelf. Dit gaf mij al een goed startpunt voor het onderzoek.

Aan het einde van week 2 heb ik veel geleerd over Gas fees en het algemene Ethereum netwerk.

### Week 3 ###
De focus van week 3 was vooral onderzoek en brainstorming. Mijn ideeen waren vooral gefocust op het ontwikkelen van toepassingen voor NFT's en andere cryptocurrencies.

In deze week ben ik ook begonnen aan mijn onderzoek naar prive blockchains.

### Week 4 ###
Na mijn onderzoek in week 3 wilde ik graag tests doen met de blockchains waar ik onderzoek naar gedaan had. Jammer genoeg waren er geen bestaande template projecten die het opzetten van een blockchain makkelijk maken.

Hierom heb ik gebruik gemaakt van docker en docker-compose om containers te maken van alle blockchain nodes. Na een aantal dagen had ik de containers werkend. Alleen loop ik tegen het probleem aan dat de containers niet met elkaar kunnen communiceren. Software wise is alles in orde met het project. Alleen ik denk dat er een probleem zit in het netwerk van de containers.

### Week 5 ###
In week 5 heb ik mij verder gefocust op het werkend krijgen van mijn blockchain testproject. Na een aantal dagen kreeg ik eindelijk het project op gang nadat ik uit had gevonden dat de containers een eigen virtueel netwerk hebben. Waardoor het verschil tussen LAN en WAN niet duidelijk is voor het programma. Ik heb dit op kunnen lossen door de poorten van de container te binden aan de computer waar het op draait. Hierdoor is er geen virtueel netwerk verbonden aan de container.

Betreft het team merk ik dat we erg beginnen af te remmen. We proberen deze week onze ideeen samen te voegen en te destilleren tot een concreet probleem of onderzoeksvraag. Maar we hebben hier moeite mee. Ik denk dat we dit voor moeten leggen aan de stakeholders volgende week om te zien wat wij hiermee kunnen doen.

Mijn focus is nu om een monitoring stack toe te voegen aan het project om de statistieken van het netwerk te bekijken.

### Week 6 ###
Tot nu toe is week 6 gericht op vooruit bewegen met het project. Als team zitten we veel te dubben over mogelijke richtingen die het project op kan gaan. Omdat we zo vol met ideeen zitten vinden we het heel lastig om een specifieke richting op te gaan.

Tim zag dit ook naar voren komen in onze demo meetings op maandag. Hij gaf ons wel goede inzichten en liet ons weten dat het oke is om een eigen richting op te gaan. Het onderzoek is tenslotte exploratief. Dus een concreet eindwerkstuk is niet verplicht.

Toch merk ik dat veel teamgenoten dit lastig vinden. Mikkel vind het heel lastig om exploratief dingen te ontwikkelen. Wat ik snap. Want zijn opleiding is erg gefocust op concrete software producten. 

Onz volgende doelstelling gaat gericht zijn op een kleinere software onderneming. Hierbij gaan ik en Mikkel kijken of we iets kunnen ontwikkelen met een Ethereum netwerk om hier meer kennis over op te doen.

### Week 7 ###
Deze week ben ik op maandag naar Denemarken gevlogen. Hier ben ik dinsdag de hele dag fysiek met mijn team aan het werken geweest. Het fysiek zien van elkaer was erg fijn. En we merkte dat we meteen productiever en gemotiveerder waren. Deze tijd samen gaf mij en Mikkel ook de mogelijkheid om af te stemmen wat we samen wilde ontwikkelen.

Na wat brainstormen kwamen we uit op een NFT verificatieprogramma. Het doel van deze app is het gebruik maken van NFTs om personen toegang te geven tot een website of exclusieve content. Hierbij logt de gebruiker in met zijn ethereum wallet en wordt deze vervolgens gecheckt op specifieke tokens. Op basis van deze tokens wordt men wel of niet toegelaten tot de website. Mocht men een "admin" token bezitten. Dan heeft hij/zij toegang tot het admin paneel waar de instellingen en "access rights" aangepast kunnen worden.

We hebben deze week een GIT repository opgezet en een eerste diagram gemaakt van het systeem.

### Week 8 ### 
Deze week zijn we verder gegaan met het uitwerken van onze applicatie.
Mikkel heeft meer ervaring met traditioneel software ontwikkeling. Hierom nam hij mij mee in de planningsfases van het ontwikkelen van software. Ik merk dat ik hier echt veel van opgestoken heb.

Ook kregen wij deze week de vraag of wij mee wilde doen met het NextM evenement. Dit is een beurs in Kopenhagen waar alle klanten van GroupM naartoe gaan om informatie en inspiratie op te doen. Het doel van ons groepje is hier om een stand te hebben met informatie en demonstraties van NFT's en blockchain. We hebben al besloten dat we mee willen doen, we moeten enkel nog een beslissing maken over wat we precies willen presenteren. Onze deadline hiervoor is volgende week maandag.

### Week 9 ###

Deze week kregen de opdracht om concrete dingen te plannen voor het NextM evenement. We hebben allemaal meerdere ideeen om mensen te trekken naar onze stand. Ik denk dat we onze app het beste kunnen showen samen met NFT's op grote schermen. Net als een soort kunstgallerij.

Ik zie het momenteel vooral als een bij dingetje wat we kunnen doen naast ons software project. Betreft het ontwikkelen zitten we nu nogsteeds in een planningsfase. Maar omdat sommige werkzaamheden niet perse volledig uitgepland moeten worden ben ik ook begonnen aan het maken van het frontend. Tot nu toe heb ik het automatisch koppelen met je Eth wallet en het maken van NFT's werkend.


### Week 10 ###
Na een meeting met Tim over het NextM project hebben we een wake-up call gehad. NextM wordt meer gezien als een einddoel van ons groepje. Dit komt deels ook doordat het deense team eerder klaar is dan ik. We hebben nu ook een concreter beeld van hoe onze stand er uit moet gaan zien. 

Ons doel gaat worden om de bezoekers een ervaring te geven van de winkel van de toekomst. Waar men niet alleen fysieke dingen kan kopen maar ook digitaal. Deze "Phygital" items zijn een concept uit een toekomst waar men buiten het echte leven met elkaar omgaat in metaverses.

We willen de bezoekers een VR ervaring aan gaan bieden van een winkel waar ze digitale goederen kunnen kopen. Hiernaast willen we kijken of we "Phygital" items weg kunnen keven aan mensen die ons bezocht hebben. Bijvoorbeeld een T-shirt waarbij je meteen de NFT versie ook ontvangt.

Qua taakverdeling heb ik besloten om iets nieuws op me te nemen. Namelijk het maken van de 3D modellen en de VR ervaring. Op kantoor heb ik toegang tot een Oculus Quest, En ik hoop hier goed gebruik van te maken.

### Week 11 ###
Ik ben nu vooral bezig met het maken van 3D modellen voor de VR ervaring. Ik heb voorheen al wel eens Blender en Cinema4D gebruikt maar het moet nog wel allemaal terugkomen.

Ons concept van een booth bij NextM veranderd wel nogsteeds continu. Dit komt mede ook doordat we nog geen duidelijkheid hebben over onze exacte locatie of toegang tot materialen.

Tevens onvangen we wat weerstand voor het uitvoeren van onze VR ervaring/expositie van de directie van het evenement. Ze lopen niet helemaal warm over het idee van een VR ervaring en expositie. Hopelijk kan Tim ze om praten of wij ons idee wat aanpassen om het wel door te laten gaan.

### Week 12 ###
Ons bezoek bij NextM gaat helaas niet door. De organisatoren vonden onze stand niet goed bij de omgeving van het evenement passen.

Maar wij willen wel onze ideeen aan anderen laten zien. Dus om toch nog iets te kunnen presenteren gaan we losse bijeenkomsten houden op iedere locatie. Hiervoor gaan we posters maken en ophangen en nogsteeds de VR ervaring aan collega's laten zien.

Mijn eerste demo van de VR ervaring was gemaakt met een programma genaamd Spatial. Helaas kon dit programma het hoge aantal 3d modellen niet aan. Om dit op te lossen ben ik overgestapt naar Unity engine.

### Week 13 ###
Deze week ging ik verder met het ontwikkelen van de VR ervaring. Unity werkend krijgen was een lastige klus maar zodra het werkt kon ik wel snel een 3d scene bouwen. Ook de interactie met de NFT's kon ik op deze manier met code bouwen. 

In deze week ben ik vooral verder gegaan met het verder indelen en werkend krijgen van de Vr demo. Het doel van deze demo is om de gebruiker te laten zien hoe interacties met virtuele goederen er uit kan zien. 
De gebruiker start in een kleine woonkamer met meubels etc. Vervolgens kan met rondlopen en interacteren met objecten in de kamer. Sommige objecten hebben een highlight en wanneer je ze van dichtbij bekijkt krijg je informatie te zien over wat voor NFT het object is.

Dit is te zien bij kunstwerken aan de muren, verzamelobjecten. En designer meubels.

### Week 14 ###
Deze week zouden de kantoren in Denemarken hun presentaties gaan doen aan de mensen op kantoor. Jammer genoeg werden de 2 mensen uit het Kopenhagen kantoor positief getest waardoor de presentatie daar verzet moet worden. Het team in Aarhus gaat op de vrijdag wel hun presentatie geven.